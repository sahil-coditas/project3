// login page js
import { $, createElement } from "./dom.js";
import { http } from "./http.js";

const mainFunction = (jwtToken, userId, userRole) => {
  adminPage();
  $('#tools').addEventListener('click', ()=>{adminToolPage(jwtToken)});
  
}

const login = () => {
  const loginCredentials = { username: '', password: '' };
  const main = createElement('main')('index-page', '');
  $('body')[0].innerHTML = ``;
  main.innerHTML = `<section class="login-image">
<img src="img-01.png" class="image-position" alt="img">
</section>

<section class="login-section-container">
<div class="login-form-heading">
<h1>Login</h1>
</div>
<section class="form-style">
<div class="username-filed">
<input type="text" class="format" id="username" placeholder="&#61447; UserId">
</div>
<div class="password-filed">
<input type="password" class="format" id="password" placeholder="&#61475; Password" style="font-family: FontAwesome;">
</div>
<button class="button-format" id="login">Log In</button>
</section>
</section>`;
  $('body')[0].appendChild(main);
  const Login = $('#login');
  Login.addEventListener('click', async () => {
    try {
      const username = $('#username').value;
      const password = $('#password').value;
      loginCredentials["username"] = username;
      loginCredentials["password"] = password;
      const { jwtToken, userId, userRole } = await http.post('authenticate', loginCredentials);
      if (jwtToken !== null) {
        mainFunction(jwtToken, userId, userRole);
      }
    } catch (e) {
    }
  });
}

// function onEdit() {
//   const workerEditBtn = document.querySelector('#worker-edit-btn');
//   workerEditBtn.addEventListener('click', () => {

//     const editformContainer = document.createElement('section');
//     editformContainer.classList.add('modal-container');
//     editformContainer.innerHTML = `
//     <div class="modal-content">
// <form class="form-container" method="PUT">
//                     <label for="id" class="item">Id</label>
//                     <input type="text" name="id">
//                     <label for="customerName" class="item">Customer Name</label>
//                     <input type="text" name="customerName">
//                     <label for="order" class="item">Order</label>
//                     <input type="text" name="order">
//                     <label for="total" class="item">Total</label>
//                     <input type="text" name="total">
//                     <label for="status" class="item">Status</label>
//                     <input type="text" name="status">
//                     <button type="submit" class="submit-btn-modal">Submit</button>
//                     <button class="xmark" id="xmarkForEdit">&times;</button>
//                 </form>
//                 </div>
// `;
//     document.querySelector('body').appendChild(editformContainer);
//   })
// }
// onEdit();


// function workerAddOrder() {
//   const workerAddBtn = document.querySelector('#worker-add-btn');
//   workerAddBtn.addEventListener('click', () => {


//     const addformContainer = document.createElement('section');
//     addformContainer.classList.add('modal-container');
//     addformContainer.innerHTML = `
//     <div class="modal-content">
//     <form class="form-container" method="POST">
//                     <label for="id" class="item">Id</label>
//                     <input type="text" name="id">
//                     <label for="customerName" class="item">Customer Name</label>
//                     <input type="text" name="customerName">
//                     <label for="order" class="item">Order</label>
//                     <input type="text" name="order">
//                     <label for="total" class="item">Total</label>
//                     <input type="text" name="total">
//                     <label for="status" class="item">Status</label>
//                     <input type="text" name="status">
//                     <button type="submit" class="submit-btn-modal">Submit</button>
//                     <button class="xmark" id="xmarkForAdd">&times;</button>
//                 </form>
//                 </div>
// `;
//     document.querySelector('body').appendChild(addformContainer);
//   })
// }
// workerAddOrder();
login();

const adminPage = ()=>{
  const main = createElement('main')('admin-page', '');
  $('body')[0].innerHTML = ``;
  main.innerHTML = ` <section class="admin-image">
  <img src="img-022.png" class="image-position" alt="img">
</section>
<div class="admin-page-buttons-container">
<button class="buttons" id="tools">Tools</button>
<button class="buttons" id="workers">Workers</button>
<button class="buttons" id = "orders">Orders</button>
</div>`;
  $('body')[0].appendChild(main);
}


const adminToolPage = async (jwtToken) =>{

  try{
    const section = createElement('section')('adminToolDisplay','');
    $('body')[0].innerHTML=``;
    const response = await http.get('admin/getTools', {Authorization : `Bearer ${jwtToken}`});
    console.log(response);
      $('body')[0].innerHTML = ``;
      for(user of response){
      section.innerHTML = `
      <div class="admin-add-btn">
                 <button class="admin-tools-page-add-btn" id="AdminToolAddBtn">ADD</button>
                 </div>
                 <table>
                     <th>ToolId</th>
                     <th>ToolName</th>
                     <th>ToolSize</th>
                     <th>ToolPrice</th>
                     <th></th>
                     <tr>
                         <td>${user.toolId}</td>
                         <td>${user.toolName}</td>
                         <td>${user.toolSize}</td>
                         <td>${user.toolPrice}</td>
                         <td>
                             <button class="admin-edit-btn" id="adminEditBtn">Edit</button>
                             <button class="admin-delete-btn">Delete</button>
                         </td>
                     </tr>
                 </table>
      `;
      $('body')[0].appendChild(section);
  }
 


    // main.innerHTML=`
    //
    //           <div class="admin-add-btn">
    //           <button class="admin-tools-page-add-btn" id="AdminToolAddBtn">ADD</button>
    //           </div>
    //           <table>
    //               <th>ToolId</th>
    //               <th>ToolName</th>
    //               <th>ToolSize</th>
    //               <th>ToolPrice</th>
    //               <th></th>
    //               <tr>
    //                   <td>1</td>
    //                   <td>hammer</td>
    //                   <td>9</td>
    //                   <td>100rs</td>
    //                   <td>
    //                       <button class="admin-edit-btn" id="adminEditBtn">Edit</button>
    //                       <button class="admin-delete-btn">Delete</button>
    //                   </td>
    //               </tr>
    //           </table>
    //
    // `;
  //   $('#AdminToolAddBtn').addEventListener('click', ()=>{adminToolPageAddBtn()});
   }catch(e){
     console.log(e);
  }
 
}








const adminToolPageAddBtn = () => {
  const popup = createElement('div')('popup', '');
  const form = createElement('section')('admin-tools-page-add','');
  form.innerHTML=`
        <i class="fa fa-times fa-2x" id="cross" aria-hidden="true"></i>
        <label for="tool-id">ToolId</label>
        <input type="number" name="tool-id" id="tool-id">
        <label for="tool-name">ToolName</label>
        <input type="text" name="tool-name" id="tool-name">
        <label for="tool-size">ToolSize</label>
        <input type="text" name="tool-size" id="tool-size">
        <label for="tool-price">ToolPrice</label>
        <input type="number" name="tool-price" id="tool-price">
        <input type="submit" name="submit" id = "submit" value="SUBMIT"
        onclick="ValidateEmail(document.formcontact1.email)">
  `;
  popup.appendChild(form);

  $('body')[0].appendChild(popup);

  $('.admin-tools-page-add')[0].querySelector('#submit').addEventListener('click', ()=>{
    $('body')[0].removeChild(popup);
  })

  $('.admin-tools-page-add')[0].querySelector('#cross').addEventListener('click', ()=>{
    $('body')[0].removeChild(popup);
  })
}

{
  function ValidateEmail(event, inputText){
     event.preventDefault();
  }
}
