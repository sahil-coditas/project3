export const AdminForm = `<main id="login-body">  
<section class="login-image">
<img src="img-01.png" class="image-position" alt="img">
</section>

<section class="login-section-container" id="login-section">
<div class="login-form-heading">
<h1>Login</h1>
</div>
<form class="form-style" id="login-form">
<div class="username-filed">
<input type="text" class="format" id="username" placeholder="&#61447; UserId">
</div>
<div class="password-filed">
<input type="password" class="format" id="password" placeholder="&#61475; Password" style="font-family: FontAwesome;">
</div>
<button class="button-format" id="login">Log In</button>
</form>
</section>
</main>`