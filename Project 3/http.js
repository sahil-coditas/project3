class Http {
    #URL = 'https://0875-103-169-241-128.in.ngrok.io/'

    async send(endpoint, authorize = {},  options = {},  data = null) {
        try {
            options = { 
                ...options,
                headers: {
                    'Content-Type': 'application/json',
                    "ngrok-skip-browser-warning": "1234",
                    ...authorize
                },
                body: data ? JSON.stringify(data) : null  
            }
            console.log(`${this.#URL}${endpoint}`, options);
            const response = await fetch(`${this.#URL}${endpoint}`, options);
            console.log(`${this.#URL}${endpoint}`, options);
            const data1 = await response.json();

            return data1;
        } catch (e) {
            console.log(e);
        }
    }

    async get(endpoint, authorize = {}) {
        return await this.send(endpoint, authorize);
    }

    async delete(endpoint, authorize = {}) {
        return await this.send(endpoint, authorize, { method: 'DELETE' }, );
    }

    async post(endpoint, data, authorize = {}) {
        return await this.send(endpoint, authorize, { method: 'POST'}, data)
    } 
    
    async put(endpoint, authorize = {}, data) {
        return await this.send(endpoint, authorize, { method: 'PUT' }, data)
    } 
}

export const http = new Http();