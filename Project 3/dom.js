export const $ = (selector)=>{
    if(selector[0]==='#')
    return document.querySelector(selector);
    else return document.querySelectorAll(selector);
}

export const createElement = (elementType)=>{
   const type = elementType
    return (classList, inner)=>{
        const element =  document.createElement(type); 
        if(classList!=='')
        element.classList.add(classList);
        if(typeof inner ==='string')
        element.innerHTML = inner;
        else
        element.appendChild(inner);
        return element;
    }
}
