
import { adminPageHtml } from "./adminPage.js";
import { adminWorkerHtml } from "./adminWorker.js";
import { adwkaddbtn } from "./adminWorkerAddBtn.js";
import { $ } from "./dom.js";
import {AdminForm} from "./loginPage.js";
let token = "";
function main() {
    loginUser();
}

main();

function loginUser() {
    const body = document.getElementById('body');
    body.innerHTML = AdminForm;

    const loginForm = document.getElementById("login-form");
    console.log(loginForm);
    loginForm.addEventListener("submit", (e) => {
        e.preventDefault();
        const loginuserName = document.getElementById('username').value;
        const userPassword = document.getElementById('password').value;
        const data = {
            username: loginuserName,
            password: parseInt(userPassword)
        }
        console.log(data);
        postUsers(data);
        adminPage();
    });
}
const postUsers = async (data) => {
    try {
        const response = await fetch("http://5332-103-51-153-190.ngrok.io/authenticate", {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                "Authorization": "jwtToken",
                "ngrok-skip-browser-warning": "1234",
                'Content-type': 'application/json; charset=UTF-8'
            }),
        });
        const newData = await response.json();
        token=newData.jwtToken;
        console.log(token);
        return token;
    }
    catch (e) {
        console.log(e);
    }
}

function adminPage(){
    const bodyEl = document.querySelector("#body");
    bodyEl.innerHTML = adminPageHtml;
    const workersButton = document.getElementById('workers');
    const toolsButton = document.getElementById('tools');
    const ordersButton = document.getElementById('orders');

    
    workersButton.addEventListener('click', () => {
    adminWorker();
})
//     ordersButton.addEventListener('click',()=>{
//     getOrderTable();
// })
}

 async function adminWorker (){
    try{

        const data = await getWorkers();
        const bodyEl = document.querySelector("#body");
        bodyEl.innerHTML=adminWorkerHtml;
        const tbody = document.querySelector("#tbody");
    
        data.forEach((user) => {
            const tableRow = document.createElement("tr");
            tableRow.innerHTML = `
              <td>${user.workerId}</td>
              <td>${user.workerName}</td>
              <td>${user.workerUsername}</td>
              <td>${user.workerSalary}</td>
              <td>
                <button class="admin-edit-btn" id="adminEditBtn">Edit</button>
                <button class="admin-delete-btn">Delete</button>
             </td>
        `
            const add = $('#add-button').addEventListener('click', ()=>{
                $('body')[0].innerHTML += adwkaddbtn;
                $('#submit').addEventListener('click',async ()=>{
                    try{
                        await addWorker();
                        $('body')[0].removeChild($('.popup-container')[0]);
                    }catch(e){
                        console.log(e);
                    }
                } )
                $('#cross').addEventListener('click', ()=>{

                
                    $('body')[0].removeChild($('.popup-container')[0]);
                })
            })
            tbody.append(tableRow);
        });
    }catch(e){

    }
   
    
}

const getWorkers = async () => {
    try {
        const response = await fetch("http://5332-103-51-153-190.ngrok.io/admin/getWorkers", {
            method: 'get',
            headers: new Headers({
                "authorization": `Bearer ${token}`,
                "ngrok-skip-browser-warning": "1234",
                'Content-type': 'application/json; charset=UTF-8'
            }),
        });
        const data = await response.json();
        console.log(data);
        return data;
    }
    catch (e) {
        console.log(e);
    }
}

const addWorker = async () => {
try{
    let workerid = document.getElementById('workerId').value;
    let workername = document.getElementById('workerName').value;
    let workerusername = document.getElementById('workerUsername').value;
    let workersalary = document.getElementById('workerSalary').value;
    let workerData = {
        workerId : workerid,
        workerName : workername,
        workerUsername : workerusername,
        workerSalary : workersalary
    };
    console.log(workerData);

    fetch("http://5332-103-51-153-190.ngrok.io/admin/createWorker",{
        method:'POST',
        body:JSON.stringify(workerData),
        headers:{'Content-type': 'application/json; charset=UTF-8', 'Authorization' : `Bearer ${token}`}
    })
    
    .then(function(response){
        return response.json()
    })
    .then(function(data){
        console.log(data)
    })
}catch(e){

}
    

}







// async function adminWorkerPage(){
//     const body = document.querySelector('#add-button');
//     body.innerHTML = adminWorkerHtml;
//     const workerDiv = document.querySelector('.worker-table');
//     console.log(workerDiv);
//     const icon = document.getElementById('cross')
//     const containerE1 = document.querySelector(".popup-container");
//     const workerForm = document.querySelector('.worker-form');
//     const adminWorkersAddBtn = document.querySelector("#add-button");
//     console.log(adminWorkersAddBtn);
//     adminWorkersAddBtn.addEventListener('click', () => {
//         workerDiv.classList.add('active');
//         workerForm.classList.remove('active');
//     })
//     icon.addEventListener('click',()=>{
//         workerDiv.classList.remove('active');
//         workerForm.classList.add('active');
//     })
// }
// adminWorkerPage();

// async function onEdit(){
//     const edidbtn = document.querySelector('#adminEditBtn');
//     const 
// }





//     popup.appendChild(form);
  
//     $('body')[0].appendChild(popup);
  
//     $('.admin-tools-page-add')[0].querySelector('#submit').addEventListener('click', ()=>{
//       $('body')[0].removeChild(popup);
//     })
  
//     $('.admin-tools-page-add')[0].querySelector('#cross').addEventListener('click', ()=>{
//       $('body')[0].removeChild(popup);
//     })
//   }
  
//   {
//     function ValidateEmail(event, inputText){
//        event.preventDefault();
//     }
//   }





// async function getWorkerTable() {
//     const data = await getWorkers();
//     const bodyEl = document.querySelector("#body");
//     bodyEl.innerHTML = workerTable;
//     console.log(bodyEl);
//     const tableBody = document.querySelector("tbody");
//     data.forEach((user) => {
//         const tableRow = document.createElement("tr");
//         tableRow.innerHTML = `
//           <td>${user.workerId}</td>
//           <td>${user.workerName}</td>
//           <td>${user.workerUsername}</td>
//           <td>${user.workerSalary}</td>
//           <td><button type="submit" class="btn edit-button">Edit</button>  <button type="submit" class="btn delete-button" id="${user.enrollmentId}">Delete</button></td>
//     `
//         tableBody.append(tableRow);
//     });
//     addWorker();
// 12:14
// function addWorker() {
//     const workerTableSection = document.getElementById("worker-table");
//     console.log(workerTableSection);
//     const closeIconElement1 = document.getElementById("close-icon-1");
//     const addWorkerButton = document.getElementById("add-worker-button");
//     const addWorkerForm = document.getElementById("add-worker-form");
//     addWorkerButton.addEventListener('click', () => {
//         workerTableSection.classList.add('add-worker-active');
//         addWorkerForm.classList.remove("add-worker-active");
//     });
//     closeIconElement1.addEventListener("click", () => {
//         workerTableSection.classList.remove('add-worker-active');
//         addWorkerForm.classList.add("add-worker-active");
//     });
//     addWorkerForm.addEventListener("submit", (e) => {
//         e.preventDefault();
//         const workerName = document.getElementById('worker-name').value;
//         const workerUsername = document.getElementById('worker-username').value;
//         const workerPassword = document.getElementById('worker-password').value;
//         const workerSalary = document.getElementById('worker-salary').value;
//         fetch("http://5332-103-51-153-190.ngrok.io/admin/createWorker", {
//             method: 'POST',
//             body: JSON.stringify({
//                 workerName: workerName,
//                 workerUsername: workerUsername,
//                 workerPassword: workerPassword,
//                 workerSalary: workerSalary
//             }),
//             headers: ({
//                 "Authorization": `Bearer ${token}`,
//                 "ngrok-skip-browser-warning": "1234",
//                 'Content-type': 'application/json; charset=UTF-8'
//             }),
//         }).then(function (response) {
//             return response.json();
//         }).then(function (data) {
//             console.log(data);
//         })
//     });
// }